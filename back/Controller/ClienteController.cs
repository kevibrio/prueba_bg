using Microsoft.AspNetCore.Mvc;
using back.Models;
using back.Data;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace webservice_app.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController: ControllerBase
    {
    private IConfiguration _config;


    public ClienteController(IConfiguration config)
    {
            _config = config;
    }
    [HttpPost]
    public IActionResult GetClienteItems([FromBody] Login loginData)
    {
        try{
            List<Client> clientes = ClientData.buscarCliente(loginData);
        if(clientes.Count > 0){
            return new OkObjectResult(new ResponseAuth(){
                codigoRetorno="0001",
                mensajeRetorno="consulta correcta",
                usuario = new ResponseLoginUserPlan()
                {
                    email=clientes[0].Email,
                    nombre=clientes[0].Name,
                    telefono=clientes[0].PhoneNumber,
                    plan=clientes[0].plans,
                    
                },
                
                token = GenerateJSONWebToken(new ResponseLoginUserPlan()
                {
                    email=clientes[0].Email,
                    nombre=clientes[0].Name,
                    telefono=clientes[0].PhoneNumber,
                    plan=clientes[0].plans,
                    
                })
            });
        }else{
            return new OkObjectResult(new ResponseError(){
                codigoRetorno="0000",
                mensajeRetorno="No hay cliente con esas credenciales"
            });
        }
        }catch(Exception ex){
            Console.Write(ex);
            return new BadRequestObjectResult(new ResponseCommonError(){
                 mensajeRetorno = "No se pudo procesar su respuesta" ,
                 codigoRetorno="9999",
                 errorMessage=Convert.ToString(ex)
            });
            
        }
    }
     private string GenerateJSONWebToken(ResponseLoginUserPlan userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

             var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Email, userInfo.email),
                        new Claim(ClaimTypes.Name, userInfo.nombre),
                        new Claim("telefono", userInfo.telefono), 
                    };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials
               );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}