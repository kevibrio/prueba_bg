using Microsoft.AspNetCore.Mvc;
using back.Models;
using back.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace webservice_app.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController: ControllerBase
    {


    public ProductController()
    {
    }
    [HttpGet]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public IActionResult GetProducts()
    {
        try{
            List<Product> productos = ProductData.buscarProductos();
        if(productos.Count > 0){
            return new OkObjectResult(new ResponseProduct(){
                codigoRetorno="0001",
                mensajeRetorno="consulta Ok",
                data = productos,
            });
        }else{
            return new OkObjectResult(new ResponseError(){
                codigoRetorno="0000",
                mensajeRetorno="No hay productos existentes"
            });
        }
        }catch(Exception ex){
            Console.Write(ex);
            return new BadRequestObjectResult(new ResponseCommonError(){
                 mensajeRetorno = "No se pudo procesar su respuesta" ,
                 codigoRetorno="9999",
                 errorMessage=Convert.ToString(ex)
            });
            
        }
    }
    }
}