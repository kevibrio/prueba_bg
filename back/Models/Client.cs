using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace back.Models{
    public class Client{
        required public int ClientID {get;set;}
        required public string Email {get;set;}
        required public string Password {get;set;}
        required public string Name {get;set;}
        required public string PhoneNumber {get;set;}
        public int plans {get;set;}

    }
}