using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace back.Models{
    public class ResponseAuth{
        required public string codigoRetorno {get;set;}
        required public string mensajeRetorno {get;set;}
        required public ResponseLoginUserPlan usuario {get;set;}

        public string? token {get;set;}

    }
}