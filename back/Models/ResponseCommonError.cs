using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace back.Models{
    public class ResponseCommonError{
        required public string codigoRetorno {get;set;}
        required public string mensajeRetorno {get;set;}
        public string? errorMessage {get;set;}

    }
}