using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace back.Models{
    public class Product{
        required public int ProductID {get;set;}
        required public string Description {get;set;}
        required public double Price {get;set;}
        required public bool isActive {get;set;}
        required public string Detail {get;set;}
        required public string image {get;set;}

    }
}