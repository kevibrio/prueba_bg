using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace back.Models{
    public class ResponseProduct{
        required public string codigoRetorno {get;set;}
        required public string mensajeRetorno {get;set;}
        required public List<Product> data {get;set;}

    }
}