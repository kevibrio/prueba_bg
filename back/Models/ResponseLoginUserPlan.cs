using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace back.Models{
    public class ResponseLoginUserPlan{
        required public string email {get;set;}
        required public string nombre {get;set;}
        public int plan {get;set;}
        required public string telefono {get;set;}

    }
}