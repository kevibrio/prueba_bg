using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace back.Models{
    public class PlanList{
        required public int PlanListID {get;set;}
        required public string Name {get;set;}
        required public string Icon {get;set;}
        required public string Description {get;set;}
        required public double PricePlan {get;set;}
        required public string Frequency {get;set;}

    }
}