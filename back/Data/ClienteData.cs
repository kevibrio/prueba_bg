using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Data.SqlClient;
using back.Models;
using System.Security;
using System.Net;
using System.ComponentModel.DataAnnotations;

namespace back.Data{
    public class ClientData{
        public static List<Client>  buscarCliente(Login loginData){
                List<Client> cliente = new List<Client>();
                string connectString = "Server=localhost;Database=BGuayaquil;User Id=SA;Password=Mario64.;";
                SqlConnection oConect = new SqlConnection(connectString);
            try{
                oConect.Open();
                string query = "select c.ClientID, c.Email, c.Name , c.Password , c.PhoneNumber , "
                + " count(cp.Client_PlanID) plans  from Client c left join Client_Plan cp on c.ClientID = cp.ClientID "
                + " where c.Email = @Email and c.Password = @Password "
                + " GROUP BY c.ClientID, c.Email, c.Name , c.Password , c.PhoneNumber ";
                SqlCommand aCommand = new SqlCommand(query, oConect);
                aCommand.Parameters.AddWithValue("@Email",loginData.email);
                aCommand.Parameters.AddWithValue("@Password",loginData.password);
                
                using (SqlDataReader reader = aCommand.ExecuteReader())

                {
                    while (reader.Read()){
                        Console.Write(reader);
                        cliente.Add(new Client(){
                            ClientID = Convert.ToInt32(reader["ClientID"]),
                            Email = Convert.ToString(reader["Email"])!,
                            Name = Convert.ToString(reader["Name"])!,
                            Password = Convert.ToString(reader["Password"])!,
                            PhoneNumber = Convert.ToString(reader["PhoneNumber"])!,
                            plans = Convert.ToInt32(reader["plans"])!,
                        }
                        );
                    }
                }
                oConect.Close();
                return cliente;
            }catch(Exception ex){
                oConect.Close();
                Console.WriteLine(ex);
                return cliente;
            }
        }
    }
}